FROM python:3.9
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD [ "python", "./app.py" ]


# FROM python:3.9
# WORKDIR /code 
# COPY requirements.txt . 
# RUN pip install -r requirements.txt
# COPY src/ .
# CMD ["python", "./app.py"]