from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/')
def home():
    return "Hello My First Flask Project"

@app.route('/json')
def test():
    people = [
                {'name': 'Alice', 'birth-year': 1986},
                {'name': 'Bob', 'birth-year': 1985}
            ]
    return jsonify(people) 


if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)